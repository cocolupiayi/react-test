/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadTodos } from './redux/reducers';
import { RootState } from './redux/store';

function App() {
  const todos = useSelector((state:RootState) => state.todos);
  const dispatch = useDispatch();

  return (
    <div>
      {/* <button
        type="button"
        aria-label="Increment value"
        onClick={loadTodos}
      >
        AddTask
      </button> */}
      <button
        className="bg-sky-600 m-8 w-300 p-3"
        type="button"
        aria-label="Increment value"
        onClick={() => loadTodos(dispatch)}
      >
        loadTodosFromDB
      </button>
      {todos.map((todo) => <div>{todo.title}</div>)}
    </div>
  );
}

export default App;
