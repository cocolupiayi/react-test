/* eslint-disable @typescript-eslint/no-unused-vars */
import { createAction, createReducer, Dispatch } from '@reduxjs/toolkit';

interface Todos{
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

export const addTodos = createAction<Todos>('todos/add');
export const removeTodos = createAction<Todos>('todos/remove');
export const addMultipleTodos = createAction<Todos[]>('todos/addMultiple');

const todosReducer = createReducer([] as Todos[], (builder) => {
  builder
    .addCase(addTodos, (state, action) => {
      // "mutate" the array by calling push()
      state.push(action.payload);
    })
    .addCase(
      removeTodos,
      (state, action) => state.filter((task) => task.id !== action.payload.id)
    )
    .addCase(addMultipleTodos, (state, action) => [...state, ...action.payload]);
});

export const loadTodos = async (dispatch:Dispatch) => {
  const res = await fetch('https://jsonplaceholder.typicode.com/todos');
  const todos = await res.json() as Todos[];
  dispatch(addMultipleTodos(todos));
};

export default todosReducer;
